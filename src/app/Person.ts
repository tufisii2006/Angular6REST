export interface Person {
    id: number;
    name: string;
    email:string,
    phoneNumber:string;
    password:string;
    username:string;
  }
  
  