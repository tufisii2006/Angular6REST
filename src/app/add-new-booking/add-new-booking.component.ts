import { Component, OnInit } from '@angular/core';
import { Field } from '../Field';
import { AddBookingService } from './add-new-bookings.service'
import { Booking } from '../Booking';
import { Person } from '../Person';

@Component({
  selector: 'app-add-new-booking',
  templateUrl: './add-new-booking.component.html',
  styleUrls: ['./add-new-booking.component.css']
})
export class AddNewBookingComponent implements OnInit {

  allFields: any[];

  createdBy = {} as Person;
  booking = {} as Booking;
  field = {} as Field;
  startHour = {} as String;
  endHour = {} as String;
  date = {} as String;

  constructor(private bookingService: AddBookingService) { }

  ngOnInit() {
    this.bookingService.getAllFields().subscribe((data: any[]) => this.allFields = data);
  }

  addNewBooking(booking): void {
    booking.atendees = [];
    booking.startTime = new String(this.date + "T" + this.startHour + "+" + "0300");
    booking.endTime = new String(this.date + "T" + this.endHour + "+" + "0300");
    booking.field = this.field;

    this.bookingService.createNewBooking(this.booking).subscribe(response => {
      console.log("Booking creation successful ", response);
    },
      error => {
        console.log("Error", error);
      }
    );
  }

}
