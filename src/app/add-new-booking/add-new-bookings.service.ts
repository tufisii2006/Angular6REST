import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Field } from '../Field';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AddBookingService {
    constructor(private http: HttpClient,private cookie:CookieService) { }
    
    getAllFields<T>(): Observable<T> {
        return this.http.get<T>("http://localhost:8080/api/field/all");
    }

    createNewBooking<Booking>(booking:Booking): Observable<Booking> {
        return this.http.post<Booking>("http://localhost:8080/api/booking/create",booking,{ withCredentials: true });
    }
}
