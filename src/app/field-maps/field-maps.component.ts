import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-field-maps',
  templateUrl: './field-maps.component.html',
  styleUrls: ['./field-maps.component.css']
})
export class FieldMapsComponent implements OnInit {
  lat = 51.678418;
  lng = 7.809007;
  constructor() { }

  ngOnInit() {
  }

}
