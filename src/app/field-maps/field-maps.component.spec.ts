import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldMapsComponent } from './field-maps.component';

describe('FieldMapsComponent', () => {
  let component: FieldMapsComponent;
  let fixture: ComponentFixture<FieldMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
