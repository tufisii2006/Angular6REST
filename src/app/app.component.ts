import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Person } from './Person';
import { HttpClient } from '@angular/common/http';
import { LoginComponent } from './login/login.component'
import { LoginService } from './login/login.service';
import { Router, ActivatedRoute, RouterOutlet } from '@angular/router';
import { ApplicationService } from './app.service'
import { trigger, transition, style, animate, state } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Sports&Fun';

  private sessionUser = {} as Person;

  constructor(private appService: ApplicationService, private loginComponent: LoginComponent, private cookie: CookieService, private route: ActivatedRoute, private router: Router, private http: HttpClient, private loginSevrice: LoginService) { }

  ngOnInit() {
    this.sessionUser = this.loginComponent.sessionUser;
  }

  isLoggedIn(): boolean {
    if (this.cookie.check("SESSION") === true) {
      return true;
    }
    return false;
  }

  getAnimationData(routerOutlet: RouterOutlet) {
    const routeData = routerOutlet.activatedRouteData['animation'];
    return routeData ? routeData : 'rootPage';
  }

  canLogOut(): boolean {
    if (this.cookie.check("SESSION") == false) {
      return true;
    }
    return false;
  }

  navigateToPersonalDetails(): void {
    this.router.navigate(['/login']);
  }
  navigateToCreateBooking(): void {
    this.router.navigate(['/addBooking']);
  }
  navigateToUserBookings(): void {
    console.log("Userul logat este", this.sessionUser);
    this.router.navigate(['/userBookings']);
  }

  navigateToInvite(): void {
    this.router.navigate(['/invite']);
  }

  navigateToLogin(): void {
    this.router.navigate(['/login']);
  }
  navigateToRegister(): void {
    this.router.navigate(['/register']);
  }
  navigateToHome(): void {
    this.router.navigate(['/home']);
  }

  navvigateToGoogleMapsField(): void {
    this.router.navigate(['/mapsField']);
  }

  logout() {
    this.appService.logout().subscribe(response => {
      console.log("Logout", response);
      window.location.reload();
    })
  }

}

