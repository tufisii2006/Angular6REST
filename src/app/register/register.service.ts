import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Person } from '../Person';

@Injectable()
export class PersonService {
    constructor(private http: HttpClient) { }
    
    addPerson(person: Person): Observable<Person> {
        return this.http.post<Person>("http://localhost:8080/api/person/register", person,{ withCredentials: true });
    }
}
