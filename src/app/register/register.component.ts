import { Component, OnInit } from '@angular/core';
import { Person } from '../Person';
import { PersonService } from './register.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  person = {} as Person;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private personService: PersonService,
  private route: ActivatedRoute, private router: Router) { }
  private nrOfRegisterAttempts = 0;

  private successMessage: string;
  private errorMessage: string;
  ngOnInit() {
  }

  addPerson(person): void {
    this.successMessage = null;
    this.errorMessage = null;
    this.personService.addPerson(person).subscribe(response => {
    this.successMessage = "Succesfull registration! Proced to login page";
    console.log("Person register successful ", response);
    setTimeout(() =>
    {
        this.router.navigate(['/login']);
    },
    3000);
    },
      response => {
        console.log("Error", response);
        if (response.error.message){
          this.errorMessage = response.error.message;
        }
        else{
        this.errorMessage = "Something went wrong! Please try again!";
        }

      }
    );
  }



}
