import { Person } from "./Person";
import { Field } from "./Field";

export interface Booking {
    id: number;
    createdBy: Person;
    atendees:Array<Person>;
    field:Field;
    startTime:string;
    endTime:string;
  }
  
  