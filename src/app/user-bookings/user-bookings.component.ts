import { Component, OnInit } from '@angular/core';
import { Booking } from '../Booking';
import { UserBookingsService } from './user-bookings.service'
import { BookingResponse } from '../BookingResponse';

@Component({
  selector: 'app-user-bookings',
  templateUrl: './user-bookings.component.html',
  styleUrls: ['./user-bookings.component.css']
})
export class UserBookingsComponent implements OnInit {

  private sessionUserBookings: BookingResponse[];
  constructor(private userBookingService: UserBookingsService) { }

  ngOnInit() {
    this.userBookingService.getAllBookingsOfAPerson(1).subscribe((data: BookingResponse[]) => this.sessionUserBookings = data);
  }

  canShowBookings() {
    if (this.sessionUserBookings) {
      return false;
    }
    return true;
  }

}
