import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UserBookingsService {

    constructor(private http: HttpClient) { }
    
    getAllBookingsOfAPerson<T>(personId): Observable<T> {
        return this.http.get<T>("http://localhost:8080/api/booking/allBookings/"+personId,{ withCredentials: true });
    }

}
