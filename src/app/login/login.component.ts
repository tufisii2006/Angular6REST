import { Component, OnInit } from '@angular/core';
import { Person } from '../Person';
import { LoginService } from './login.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   public person = {} as Person;
  returnUrl: string;
  private errorMessage: string;
  public sessionUser;


  public payPalConfig?: PayPalConfig;

  private initConfig(): void {
    this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
      commit: true,
      client: {
        sandbox: 'AdzltCoI4wzL8a6Nm7PVqwvMn1C5cMWYvyc14k06z7Xvo6i215SyW58rSMq6XPw4sk02duUx6k7WW8_I',
      },
      button: {
        label: 'checkout',
      },
      onPaymentComplete: (data, actions) => {
        console.log('OnPaymentComplete');
      },
      onCancel: (data, actions) => {
        console.log('OnCancel');
      },
      onError: (err) => {
        console.log('OnError');
      },
      transactions: [{
        amount: {
          currency: 'USD',
          total: 1.1
        }
      }]
    });
  }

  constructor(private loginService: LoginService,
    private cookie: CookieService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.initConfig();
  }

  login(person): void {
    this.loginService.login(person).subscribe(
      response => {
        this.sessionUser = response.person;
        console.log('Login successful ', response.person.name);
        this.router.navigateByUrl('home');
        console.log('Login successful ', response);
      },
      error => {
        console.log('Error', error);
        if (error.error) {
          this.errorMessage = error.error;
        }
        else {
          this.errorMessage = 'Something wrong happened';
        }
      }
    );
  }

}
