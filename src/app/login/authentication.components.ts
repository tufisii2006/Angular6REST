import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private cookie: CookieService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let a=1;
        if (this.cookie.check("SESSION")==true) {
            return true;
        }
        this.router.navigate(['/login'], {
            queryParams: {
                return: state.url
            }
        });
        return false;

    }
}