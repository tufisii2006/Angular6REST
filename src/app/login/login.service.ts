import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Person } from '../Person';
import { LoginToken } from '../LoginToken';

@Injectable()
export class LoginService {
    constructor(private http: HttpClient) { }

    login(person: Person): Observable<LoginToken> {

        return this.http.post<LoginToken>("http://localhost:8080/api/login", person, { withCredentials: true });
    }

}
