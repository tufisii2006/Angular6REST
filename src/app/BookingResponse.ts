import { Person } from "./Person";
import { Field } from "./Field";

export interface BookingResponse {
    id: number;
    createdBy: Person;
    atendees:Array<Person>;
    field:Field;
    startTime:Date;
    endTime:Date;
  }
  
  