export interface Field {
    id: number;
    name: string;
    address: string;
    capacity:number,
    sportType:string;
    details:string;
  }
  
  