import { Person } from "./Person";

export interface LoginToken {
    autenticationToken: string;
    person:Person;
  }
  