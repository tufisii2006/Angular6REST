import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApplicationService {
    constructor(private http: HttpClient) { }
    
    logout(): Observable<String>  {
    return  this.http.get<String>("http://localhost:8080/api/logout",{ withCredentials: true });
    }
}
