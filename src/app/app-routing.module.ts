import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { RegisterComponent } from './register/register.component'
import { AddNewBookingComponent } from './add-new-booking/add-new-booking.component';
import { AuthGuardService } from './login/authentication.components';
import { MainPageComponent } from './main-page/main-page.component';
import { AvailableFieldsComponent } from './available-fields/available-fields.component';
import { UserBookingsComponent } from './user-bookings/user-bookings.component';
import { InvitationLinkComponent } from './invitation-link/invitation-link.component';
import { FieldMapsComponent } from './field-maps/field-maps.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: MainPageComponent, },
  { path: 'register', component: RegisterComponent },
  { path: 'addBooking', component: AddNewBookingComponent, },
  { path: 'home', component: MainPageComponent, },
  { path: 'availableFields', component: AvailableFieldsComponent, },
  { path: 'userBookings', component: UserBookingsComponent, },
  { path: 'invite', component: InvitationLinkComponent, },
  { path: 'mapsField', component: FieldMapsComponent, },
];
//canActivate: [AuthGuardService]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: [],
})

export class AppRoutingModule {

}
